package org.serpeninov.hw2;

import java.util.Scanner;

public class palindrome {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter: ");
        String input = scanner.nextLine();

        if (Palindrome(input)) {
            System.out.println("This is palindrome");
        } else {
            System.out.println("This isn't palindrome");
        }

        scanner.close();
    }
    public static boolean Palindrome(String input) {
        String string = input.replaceAll("\\s+", "").toLowerCase();

        int a = 0;
        int b = string.length() - 1;

        while (a < b) {
            if (string.charAt(a) != string.charAt(b)) {
                return false;
            }
            a++;
            b--;
        }
        return true;
    }
}