package org.serpeninov.hw1;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        double first;
        double second;
        char ar_op;
        double res;

        Scanner calc = new Scanner(System.in);
        System.out.print("\n!!!Write the decimal through \",\"\nEnter first value : ");
        first = calc.nextDouble();
        System.out.print("\nEnter arithmetic operation\n'+' || '-' || '*' || '/' || '%' : ");
        ar_op = calc.next().charAt(0);
        System.out.print("\n!!!Write the decimal through \",\"\nEnter second value : ");
        second = calc.nextDouble();

        switch (ar_op) {
            // '+'
            case '+':
                res = first + second;
                System.out.print("\nResult: " + first);
                System.out.print(" + " + second + " = " + res + "\n");
                break;
            // '-'
            case '-':
                res = first - second;
                System.out.print("\nResult: " + first);
                System.out.print(" - " + second + " = " + res + "\n");
                break;
            // '*'
            case '*':
                res = first * second;
                System.out.print("\nResult: " + first);
                System.out.print(" * " + second + " = " + res + "\n");
                break;
            // '/'
            case '/':
                res = first / second;
                if (second != 0){
                    System.out.print("\nResult: " + first);
                    System.out.print(" / " + second + " = " + res + "\n");
                }else {
                    System.out.println("\nERROR!!! You can't divide on 0!");
                }
                break;
            // '%'
            case '%':
                res = first % second;
                if (second != 0){
                    System.out.print("\nResult: " + first);
                    System.out.print(" % " + second + " = " + res + "\n");
                }else {
                    System.out.println("\nERROR!!! You can't divide on 0!");
                }
                break;
            // other
            default:
                System.out.println();
                System.out.println("ERROR!!! This is not an arithmetic operation");
        }
    }
}