package org.serpeninov.hw3;

public class palindrome_array {
    public static boolean arrayPalindrome(int[] array) {
        for (int i = 0, k = array.length - 1; i < k; i++, k--) {
            if (array[i] != array[k]) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        //please, manually enter the array here
        int[] array = {10, 100, 3, 3, 10, 10};

        if (arrayPalindrome(array)) {
            System.out.println("This array is palindrome");
        } else {
            System.out.println("This array isn't palindrome");
        }
    }
}
