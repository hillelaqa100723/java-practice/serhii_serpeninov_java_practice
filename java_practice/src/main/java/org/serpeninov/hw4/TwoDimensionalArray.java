package org.serpeninov.hw4;

public class TwoDimensionalArray {

    //введіть матрицю самостійно
    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        printMatrix(array);
    }

    //вивід матриці на екран
    public static void printMatrix(int[][] array) {
        int a = array.length;

        System.out.print("\nTwo dimensional array:\n");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        //головна діагональ матриці
        System.out.print("\nThe main diagonal: ");
        for (int i = 0; i < a; i++) {
            System.out.print(array[i][i]);
            if (i < a - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
        //побічна діагональ матриці
        System.out.print("\nThe side diagonal: ");
        for (int i = 0; i < a; i++) {
            System.out.print(array[a - 1 - i][i]);
            if (i < a - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
    }

}
